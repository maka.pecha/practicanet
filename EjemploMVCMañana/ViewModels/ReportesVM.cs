﻿using EjemploMVCMañana.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploMVCMañana.ViewModels
{
    public class ReportesVM
    {
        public List<CantidadPorSexoDTO> cantidadPorSexo { get; set; }
        public PersonaDTO ultimoSexoFemenino { get; set; }
    }
}