﻿using EjemploMVCMañana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploMVCMañana.ViewModels
{
    public class PersonaVM
    {
        public Persona personaModel { get; set; }
        public List<Sexo> sexoPersona { get; set; }
    }
}