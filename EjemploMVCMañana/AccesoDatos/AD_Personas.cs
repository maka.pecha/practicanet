﻿using EjemploMVCMañana.DataTransferObjects;
using EjemploMVCMañana.Models;
using EjemploMVCMañana.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EjemploMVCMañana.AccesoDatos
{
    public class AD_Personas
    {
        public static bool InsertarNuevaPersona(Persona per)
        {
            bool resultado = false;

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();
                
                string consulta = "insert into personas values(@nombre, @apellido, @edad, @telefono, @idSexo)";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@nombre", per.nombre); //asigno parametros al command
                cmd.Parameters.AddWithValue("@apellido", per.apellido);
                cmd.Parameters.AddWithValue("@edad", per.edad);
                cmd.Parameters.AddWithValue("@telefono", per.telefono);
                cmd.Parameters.AddWithValue("@idSexo", per.idSexo);
                
                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta
                cmd.ExecuteNonQuery(); //ejecuta la sentencia
                resultado = true; // si no hubo error es true
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        } 
        //public static List<Persona> ObtenerListaPersonas()
        //{
        //    List<Persona> resultado = new List<Persona>();

        //    string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

        //    SqlConnection cn = new SqlConnection(cadenaConexion);

        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand();
                
        //        string consulta = @"select * from personas";
        //        cmd.Parameters.Clear();
                
        //        cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
        //        cmd.CommandText = consulta; //esta es la sentencia

        //        //la consulta
        //        cn.Open(); //abro la conexion
        //        cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

        //        SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

        //        if(dr != null)
        //        {
        //            while (dr.Read())
        //            {
        //                Persona aux = new Persona();
        //                aux.id = int.Parse(dr["id"].ToString());
        //                aux.nombre = dr["nombre"].ToString();
        //                aux.apellido = dr["apellido"].ToString();
        //                aux.edad = int.Parse(dr["edad"].ToString());
        //                aux.telefono = dr["telefono"].ToString();

        //                resultado.Add(aux);
        //            }
        //        }

                
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        cn.Close();//si hay o no error igual se cierra la conexion
        //    }

        //    return resultado;
        //}

        public static List<PersonaDTO> ObtenerListaPersonas()
        {
            List<PersonaDTO> resultado = new List<PersonaDTO>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = @"select p.id, p.nombre, apellido, edad, telefono, s.nombre as sexo
                                    from personas p
                                    join sexos s on p.idSexo=s.id";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        PersonaDTO aux = new PersonaDTO();
                        aux.id = int.Parse(dr["id"].ToString());
                        aux.nombre = dr["nombre"].ToString();
                        aux.apellido = dr["apellido"].ToString();
                        aux.edad = int.Parse(dr["edad"].ToString());
                        aux.telefono = dr["telefono"].ToString();
                        aux.sexo = dr["sexo"].ToString();

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static Persona ObtenerPersona(int idPersona)
        {
            Persona resultado = new Persona();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "select * from personas where id = @id";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id", idPersona);

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        resultado.id = int.Parse(dr["id"].ToString());
                        resultado.nombre = dr["nombre"].ToString();
                        resultado.apellido = dr["apellido"].ToString();
                        resultado.edad = int.Parse(dr["edad"].ToString());
                        resultado.telefono = dr["telefono"].ToString();
                        resultado.idSexo = int.Parse(dr["idSexo"].ToString());
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static bool ActualizarDatosPersona(Persona per)
        {
            bool resultado = false;

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "update personas set nombre = @nombre, apellido = @apellido, edad = @edad, telefono = @telefono, idSexo = @idSexo where id = @id";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@nombre", per.nombre); //asigno parametros al command
                cmd.Parameters.AddWithValue("@apellido", per.apellido);
                cmd.Parameters.AddWithValue("@edad", per.edad);
                cmd.Parameters.AddWithValue("@telefono", per.telefono);
                cmd.Parameters.AddWithValue("@id", per.id);
                cmd.Parameters.AddWithValue("@idSexo", per.idSexo);

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta
                cmd.ExecuteNonQuery(); //ejecuta la sentencia
                resultado = true; // si no hubo error es true
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static bool EliminarPersona(Persona per)
        {
            bool resultado = false;

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "delete from personas where id = @id";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id", per.id); //asigno parametros al command

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta
                cmd.ExecuteNonQuery(); //ejecuta la sentencia
                resultado = true; // si no hubo error es true
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<Sexo> ObtenerListaSexos()
        {
            List<Sexo> resultado = new List<Sexo>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "select * from sexos";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Sexo aux = new Sexo();
                        aux.id = int.Parse(dr["id"].ToString());
                        aux.nombre = dr["nombre"].ToString();

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<CantidadPorSexoDTO> ObtenerListaCantidadPorSexo()
        {
            List<CantidadPorSexoDTO> resultado = new List<CantidadPorSexoDTO>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = @"select s.nombre, count(*) as cantidad
                                    from personas p
                                    join sexos s on p.idSexo=s.id
                                    group by s.nombre;";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        CantidadPorSexoDTO aux = new CantidadPorSexoDTO();
                        aux.nombre = dr["nombre"].ToString();
                        aux.cantidad = int.Parse(dr["cantidad"].ToString());

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static PersonaDTO ObtenerUltimoSexoFemenino()
        {
            PersonaDTO resultado = new PersonaDTO();
            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = @"select top 1 p.id, p.nombre, apellido, edad, telefono, s.nombre as sexo
                                    from personas p
                                    join sexos s on p.idSexo=s.id
                                    where idSexo = 1
                                    order by id desc";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr.Read())
                {
                    resultado.id = int.Parse(dr["id"].ToString());
                    resultado.nombre = dr["nombre"].ToString();
                    resultado.apellido = dr["apellido"].ToString();
                    resultado.edad = int.Parse(dr["edad"].ToString());
                    resultado.telefono = dr["telefono"].ToString();
                    resultado.sexo = dr["sexo"].ToString();
                }

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }
    }
}