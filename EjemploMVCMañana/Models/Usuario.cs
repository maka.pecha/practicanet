﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EjemploMVCMañana.Models
{
    public class Usuario
    {
        [Required(ErrorMessage = "El nombre es requerido")]
        public string nombreUsuario { get; set; }
        [Required(ErrorMessage = "La contraseña es requerida")]
        public string password { get; set; }
    }
}