﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EjemploMVCMañana.Models
{
    public class Persona
    {
        public int id { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El apellido es requerido")]
        public string apellido { get; set; }
        [Required(ErrorMessage = "La edad es requerido")]
        public int edad { get; set; }
        [Required(ErrorMessage = "El telefono es requerido")]
        public string telefono { get; set; }
        [Required(ErrorMessage = "El sexo es requerido")]
        public int idSexo { get; set; }

    }
}