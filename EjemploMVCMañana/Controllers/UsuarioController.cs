﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EjemploMVCMañana.Models;

namespace EjemploMVCMañana.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Usuario modelo)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Principal", "Home"); //para redireccionar a la vista Principal del controller Home

            } else
            {
                return View(modelo); //si hay errores le devuelvo la misma vista el modelo
            }
        }
    }
}