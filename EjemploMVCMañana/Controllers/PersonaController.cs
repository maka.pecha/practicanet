﻿using EjemploMVCMañana.AccesoDatos;
using EjemploMVCMañana.DataTransferObjects;
using EjemploMVCMañana.Models;
using EjemploMVCMañana.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EjemploMVCMañana.Controllers
{
    public class PersonaController : Controller
    {
        // GET: Persona
        public ActionResult AltaPersona()
        {
            PersonaVM vm = new PersonaVM();
            vm.sexoPersona = AD_Personas.ObtenerListaSexos();

            return View(vm);
        }
        // para hacer el post de la nueva persona y ver la lista
        [HttpPost]
        public ActionResult AltaPersona(PersonaVM model)
        {
            if (ModelState.IsValid)
            {
                bool resultado = AD_Personas.InsertarNuevaPersona(model.personaModel);
                if (resultado)
                {
                    return RedirectToAction("ListadoPersonas", "Persona");
                } else {
                    return View(model);
                }
            } else 
            {
                model.sexoPersona = AD_Personas.ObtenerListaSexos();
                return View(model);
            }
        }
        // get de la lista
        public ActionResult ListadoPersonas()
        {
            List<PersonaDTO> lista = AD_Personas.ObtenerListaPersonas();
            return View(lista);
        }
        // get de una sola persona para hacer el update
        public ActionResult DatosPersona(int idPersona)
        {
            List<Sexo> listaSexos = AD_Personas.ObtenerListaSexos();
            List<SelectListItem> itemsCombo = listaSexos.ConvertAll(d => //el combo solo se carga con la lista de tipo SelectListItem
            {
                return new SelectListItem()
                {
                    Text = d.nombre,
                    Value = d.id.ToString(),
                    Selected = false
                };
            });

            Persona resultado = AD_Personas.ObtenerPersona(idPersona);

            foreach (var item in itemsCombo)
            {
                if (item.Value.Equals(resultado.idSexo.ToString()))
                {
                    item.Selected = true;
                    break;
                }
            }
            ViewBag.items = itemsCombo; //para pasar datos desde el controlador a la vista

            ViewBag.Nombre = resultado.nombre + " " + resultado.apellido;
            return View(resultado);
        }
        // para hacer el update
        [HttpPost]
        public ActionResult DatosPersona(Persona model)
        {
            if (ModelState.IsValid)
            {
                bool resultado = AD_Personas.ActualizarDatosPersona(model);
                if (resultado)
                {
                    return RedirectToAction("ListadoPersonas", "Persona");
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                List<Sexo> listaSexos = AD_Personas.ObtenerListaSexos();
                List<SelectListItem> itemsCombo = listaSexos.ConvertAll(d => //el combo solo se carga con la lista de tipo SelectListItem
                {
                    return new SelectListItem()
                    {
                        Text = d.nombre,
                        Value = d.id.ToString(),
                        Selected = false
                    };
                });

                ViewBag.items = itemsCombo; //para pasar datos desde el controlador a la vista
                return View(model);
            }
        }
        //get para eliminar
        public ActionResult EliminarPersona(int idPersona)
        {
            Persona resultado = AD_Personas.ObtenerPersona(idPersona);
            return View(resultado);
        }
        // para hacer el delete
        [HttpPost]
        public ActionResult EliminarPersona(Persona model)
        {
            if (ModelState.IsValid)
            {
                bool resultado = AD_Personas.EliminarPersona(model);
                if (resultado)
                {
                    return RedirectToAction("ListadoPersonas", "Persona");
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Reportes()
        {
            List<CantidadPorSexoDTO> lista = AD_Personas.ObtenerListaCantidadPorSexo();
            PersonaDTO ultimoSexoFemenino = AD_Personas.ObtenerUltimoSexoFemenino();

            ReportesVM vm = new ReportesVM();
            vm.cantidadPorSexo = lista;
            vm.ultimoSexoFemenino = ultimoSexoFemenino;

            return View(vm);
        }
    }
}