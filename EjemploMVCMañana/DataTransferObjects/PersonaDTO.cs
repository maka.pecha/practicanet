﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploMVCMañana.DataTransferObjects
{
    public class PersonaDTO
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public int edad { get; set; }
        public string telefono { get; set; }
        public string sexo { get; set; }
    }
}